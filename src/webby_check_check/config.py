import os
from typing import Mapping, Sequence, Type, TypeVar

import attr
import dotenv
from aiokafka.helpers import create_ssl_context

T = TypeVar("T")


def config_from_dict(cls: Type[T], d: Mapping) -> T:
    """
    Build an attrs-based config object from a mapping object.

    Raise a helpful error of missing required keys if not found.  Using this should
    allow applications to die early if necessary config is not present.
    """
    missing = set()
    kwargs = {}
    for field in attr.fields(cls):
        if field.name in d:
            kwargs[field.name] = d[field.name]
        elif field.default is attr.NOTHING:
            missing.add(field.name)

    if missing:
        raise KeyError(f"Keys {missing} not found in input mapping")

    return cls(**kwargs)


def config_from_environ(cls: Type[T]) -> T:
    """
    Return an instance of the config type ``cls`` initialized from the environment.

    Will attempt to read environment variables from a ``.env`` file, as well as the
    actual environment.
    """
    dotenv.load_dotenv()
    return config_from_dict(cls, os.environ)


@attr.dataclass(frozen=True)
class Config:
    """
    Config object representing necessary producer input.
    """

    KAFKA_SSL_CAFILE: str
    KAFKA_SSL_KEYFILE: str
    KAFKA_SSL_CERTFILE: str
    KAFKA_BOOTSTRAP_SERVERS: Sequence[str] = attr.ib(
        converter=lambda val: [x.strip() for x in val.split(",")]
    )

    POSTGRES_DBNAME: str
    POSTGRES_HOST: str
    POSTGRES_PORT: int = attr.ib(converter=int)
    POSTGRES_USERNAME: str
    POSTGRES_PASSWORD: str

    POSTGRES_SSL_CAFILE: str

    TARGET_TOPIC: str = "cool_beans"
    NUM_DOWNLOADERS: int = 5
    NUM_PUBLISHERS: int = 10

    from_environ = classmethod(config_from_environ)

    def kafka_kwargs(self):
        return {
            "bootstrap_servers": self.KAFKA_BOOTSTRAP_SERVERS,
            "security_protocol": "SSL",
        }

    def kafka_ssl_context(self):
        return create_ssl_context(
            cafile=self.KAFKA_SSL_CAFILE,
            certfile=self.KAFKA_SSL_CERTFILE,
            keyfile=self.KAFKA_SSL_KEYFILE,
        )

    def postgres_ssl_context(self):
        return create_ssl_context(
            cafile=self.POSTGRES_SSL_CAFILE,
        )

    def postgres_connect_kwargs(self):
        return dict(
            host=self.POSTGRES_HOST,
            port=self.POSTGRES_PORT,
            user=self.POSTGRES_USERNAME,
            password=self.POSTGRES_PASSWORD,
            database=self.POSTGRES_DBNAME,
            ssl=self.postgres_ssl_context(),
        )
