DROP TABLE IF EXISTS scans;
CREATE TABLE scans
    ( id SERIAL PRIMARY KEY
    , ts BIGINT NOT NULL
    , url TEXT NOT NULL
    , code SMALLINT NOT NULL
    , response_time FLOAT NOT NULL
    , match TEXT NOT NULL

    , CONSTRAINT scans_ts_url_index UNIQUE (ts, url));
