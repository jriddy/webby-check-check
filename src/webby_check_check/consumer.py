import json
import logging

import aiokafka
import trio
import trio_asyncio
import triopg
from trio_asyncio import aio_as_trio

from webby_check_check import common, config

logger = logging.getLogger(__name__)


def bytes_to_result(bs: bytes) -> common.ScanResult:
    data = json.loads(bs.decode())
    return config.config_from_dict(common.ScanResult, data)


async def read_from_kafka(consumer, tx):
    async with tx:
        await aio_as_trio(consumer.start)()
        getone = aio_as_trio(consumer.getone)
        try:
            while True:
                # TODO: more robust error handling, issue #1
                # https://gitlab.com/jriddy/webby-check-check/-/issues/1
                msg = await getone()
                logger.debug("got record %s", msg)
                try:
                    sr = bytes_to_result(msg.value)
                except (ValueError, KeyError):
                    logger.exception("error while decoding msg: %s", msg)
                    continue
                await tx.send((msg.timestamp, sr))
        finally:
            await aio_as_trio(consumer.stop)()


async def write_to_db(conn, rx):
    dup_check = "select count(1) from scans where ts = $1 and url = $2"
    insert_stmt = (
        "insert into scans (ts, url, code, response_time, match) "
        "values ($1, $2, $3, $4, $5)"
    )
    async with conn, rx:
        async for msg in rx:
            print("storing %s", msg)
            async with conn.transaction():
                ts, sr = msg
                rows = await conn.fetch(dup_check, ts, sr.url)
                if not rows:
                    match = sr.match if isinstance(str, sr.match) else ""
                    await conn.execute(insert_stmt, ts, sr.url, sr.code, sr.time, match)


async def async_main(conf: config.Config):
    tx, rx = trio.open_memory_channel(0)
    async with trio.open_nursery() as nursery:
        consumer = aiokafka.AIOKafkaConsumer(
            conf.TARGET_TOPIC,
            group_id="group1",
            auto_offset_reset="latest",
            ssl_context=conf.kafka_ssl_context(),
            **conf.kafka_kwargs(),
        )
        dbconn = triopg.connect(**conf.postgres_connect_kwargs())
        nursery.start_soon(read_from_kafka, consumer, tx)
        nursery.start_soon(write_to_db, dbconn, rx)


def main():
    conf = config.Config.from_environ()
    trio_asyncio.run(async_main, conf)


if __name__ == "__main__":
    main()
