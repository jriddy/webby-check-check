from pathlib import Path

import trio_asyncio
import triopg

from webby_check_check import config

schema_path = Path(__file__).parent / "schema.sql"


async def async_main(conf: config.Config) -> None:
    schema_sql = schema_path.read_text()
    async with triopg.connect(**conf.postgres_connect_kwargs()) as conn:
        await conn.execute(schema_sql)


def main():
    conf = config.Config.from_environ()
    trio_asyncio.run(async_main, conf)
    print("DB sucessfully created")


if __name__ == "__main__":
    main()
