from typing import Union

import attr


@attr.dataclass
class ScanResult:
    """
    Scan result object.

    Attributes:
        url: the scanned URL
        code: HTTP return code.
        time: time, in seconds, to complete a full response.
        match: Whether the check passes.  If no pattern check given, this value will be
            a bool indicating success or failure.  If a pattern is given, this value
            will be the first matched text if found, or None if not found.
    """

    url: str
    code: int
    time: float
    match: Union[str, bool, None]


async def printall(rx):
    async with rx:
        async for value in rx:
            print(value)
