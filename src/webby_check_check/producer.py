import json
import logging
import re
import sys
from pathlib import Path
from typing import Optional

import aiokafka
import asks
import attr
import trio
import trio_asyncio
from trio_asyncio import aio_as_trio

from webby_check_check import config
from webby_check_check.common import ScanResult

logger = logging.getLogger(__name__)


async def scan_url(url: str, pat: Optional[re.Pattern] = None) -> ScanResult:
    """
    Scan the given URL, returning a :class:`ScanResult` on completion.
    """
    start = trio.current_time()
    resp = await asks.get(url)
    end = trio.current_time()
    code = resp.status_code

    if pat is not None:
        m = pat.search(resp.text)
        match = m and m.group(0)
    else:
        match = code == 200

    return ScanResult(url, code, end - start, match)


@attr.dataclass
class CheckSpec:
    """
    Specification for how often to check a url.

    Attributes:
        url: URL to poll periodically
        pat: Pattern to look for in a page
        frequency: Default polling frequency in seconds
    """

    DEFAULT_FREQUENCY = 5 * 60

    url: str
    pat: Optional[re.Pattern] = attr.ib(default=None, converter=lambda pat: pat and re.compile(pat))
    frequency: float = DEFAULT_FREQUENCY


async def input_producer(spec, tx):
    async with tx:
        while True:
            await tx.send(spec)
            await trio.sleep(spec.frequency)


async def check_url(rx, tx):
    async with tx:
        async for spec in rx:
            try:
                sr = await scan_url(spec.url, spec.pat)
            except OSError:
                # Likely at networking error, not a successful HTTP response
                # TODO: something better than dropping this
                logger.exception(f"unable to access url {spec.url!r}")

            await tx.send(sr)


def result_to_bytes(sr: ScanResult) -> str:
    return json.dumps(attr.asdict(sr)).encode()


async def send_to_kafka(rx, topic, kafka_producer):
    await aio_as_trio(kafka_producer.start)()
    send_and_wait = aio_as_trio(kafka_producer.send_and_wait)
    async with rx:
        try:
            async for result in rx:
                print("sending:", result)
                msg = result_to_bytes(result)
                with trio.move_on_after(2):
                    await send_and_wait(topic, msg)
                print("Sent")
        finally:
            await aio_as_trio(kafka_producer.stop)()


async def main_loop(check_specs, conf: config.Config):
    async with trio.open_nursery() as nursery:
        input_tx, input_rx = trio.open_memory_channel(0)
        process_tx, process_rx = trio.open_memory_channel(0)
        async with input_tx, input_rx, process_tx, process_rx:
            for spec in check_specs:
                nursery.start_soon(input_producer, spec, input_tx.clone())

            for _ in range(conf.NUM_DOWNLOADERS):
                nursery.start_soon(check_url, input_rx.clone(), process_tx.clone())

            for _ in range(conf.NUM_PUBLISHERS):
                producer = aiokafka.AIOKafkaProducer(
                    ssl_context=conf.kafka_ssl_context(),
                    **conf.kafka_kwargs(),
                )
                # TODO: supervise/timeout producers
                nursery.start_soon(
                    send_to_kafka, process_rx.clone(), "cool_beans", producer
                )


def parse_specs():
    if len(sys.argv) != 2:
        usage()
    elif sys.argv[1] in ('-h', '--help'):
        usage(0)

    try:
        with Path(sys.argv[1]).open('r') as file:
            data = json.load(file)
        return [config.config_from_dict(CheckSpec, d) for d in data['specs']]
    except (ValueError, TypeError, KeyError):
        usage()


def usage(exit_code=1):
    print("usage: PROGRAM SPEC.json")
    print()
    print("example input JSON:")
    print((Path(__file__).parent / "example.json").read_text())
    raise SystemExit(exit_code)


def main():
    asks.init("trio")
    conf = config.Config.from_environ()
    specs = parse_specs()
    print(specs)
    trio_asyncio.run(main_loop, specs, conf)


if __name__ == "__main__":
    main()
