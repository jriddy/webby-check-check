FROM python:3.8-slim-buster

RUN export DEBIAN_FRONTEND=interactive \
    && apt-get update \
    && apt-get install -yq --no-install-recommends \
        curl \
    && apt-get clean \
    && rm -r /var/lib/apt/lists/*

ARG USER=code
ARG HOME="/home/${USER}"
RUN useradd -md "${HOME}" "${USER}"
USER "${USER}"
WORKDIR "${HOME}"

# install poetry
RUN curl -sSL https://raw.githubusercontent.com/python-poetry/poetry/master/get-poetry.py | python -
ENV PATH="${HOME}/.poetry/bin:${PATH}"

COPY pyproject.toml ./
RUN poetry install --no-root

COPY ./ ./
RUN poetry install
