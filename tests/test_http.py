import re

import pytest

from webby_check_check import producer


@pytest.mark.internet_access
async def test_example_com():
    """
    Test that we can scan example.com.

    Since this requires live internet access, this test could be a bit flaky, but that's
    the nature of a real, unmocked test here.
    """
    sr = await producer.scan_url("https://example.com")
    assert (sr.code, sr.match) == (200, True)


async def test_simple_patterns(mocker):
    m_get = mocker.patch("asks.get")
    m_resp = mocker.Mock()
    m_resp.text = "Everything is <strong>OK</strong>"
    m_get.return_value = m_resp

    sr = await producer.scan_url("ignored url", re.compile(r"<.*?>\s*OK\s*</.*?>"))
    assert sr.match == "<strong>OK</strong>"
