import re

import pytest
import trio

from webby_check_check import producer

_DUMMY_URL = "https://example.com/"


@pytest.mark.parametrize(
    "url, pat, expected_output",
    [
        (_DUMMY_URL, None, producer.CheckSpec(_DUMMY_URL)),
        (_DUMMY_URL, ".*", producer.CheckSpec(_DUMMY_URL, re.compile(".*"))),
        (
            _DUMMY_URL,
            re.compile(r"all\s+clear", re.I),
            producer.CheckSpec(_DUMMY_URL, re.compile(r"all\s+clear", re.I)),
        ),
    ],
)
def test_check_specs_from_strings(url, pat, expected_output):
    assert producer.CheckSpec(url, pat) == expected_output


@pytest.mark.internet_access
async def test_check_url_external():
    items = []

    async def produce_item(x, tx):
        async with tx:
            await tx.send(x)

    async def collect_item(rx):
        async with rx:
            async for x in rx:
                items.append(x)

    tx0, rx0 = trio.open_memory_channel(0)
    tx1, rx1 = trio.open_memory_channel(0)
    async with trio.open_nursery() as nursery:
        cs = producer.CheckSpec(_DUMMY_URL)
        nursery.start_soon(produce_item, cs, tx0)
        nursery.start_soon(producer.check_url, rx0, tx1)
        nursery.start_soon(collect_item, rx1)

    sr = items[0]
    assert (sr.code, sr.match) == (200, True)
