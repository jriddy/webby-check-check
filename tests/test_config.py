import attr
import pytest

from webby_check_check import config


@attr.dataclass
class SampleConfig:
    a: str
    b: str
    c: str = "c"

    from_dict = classmethod(config.config_from_dict)


def test_config_from_dict_defaulting():
    tc = SampleConfig.from_dict({"a": 1, "b": 2})
    assert tc == SampleConfig(1, 2)


def test_config_from_dict_missing_two_args():
    with pytest.raises(KeyError) as e:
        _ = SampleConfig.from_dict({"c": 1})
    e_msg = str(e.value)
    assert "{'a', 'b'}" in e_msg or "{'b', 'a'}" in e_msg


def test_config_from_dict_missing_one_arg():
    with pytest.raises(KeyError) as e:
        _ = SampleConfig.from_dict({"a": 5, "c": 1})
    e_msg = str(e.value)
    assert "{'b'}" in e_msg


def test_config_from_dict_overrides_default():
    tc = SampleConfig.from_dict({"a": 1, "b": 2, "c": "value"})
    assert tc == SampleConfig(1, 2, "value")
