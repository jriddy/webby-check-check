# Webby Check-Check

Webby Check-Check is a utility to check the status of websites, and publish results to
a Kafka server.  Webby Check-Check provides a consumer as well that can read the Kafka
topics and publish them idempotently to a Postgres database.

## Installation

This package uses `poetry`.  See its [docs](https://python-poetry.org/docs/#introduction)
for installation info on it.  Once poetry is installed, run the following in the root
directory of this repository:

```
poetry install
```

This should create a virtualenv and install all needed dependencies.


## Configuration

Webby Check-Check needs Kafka and Postgres configuration to run correctly.  It accepts
this configuration via environment variable, or via an `.env` file in the current directory
of the process.  Environment variables should override the values in the `.env` file.
An example `.env` file looks like this:

```
KAFKA_BOOTSTRAP_SERVERS="kafka-1-jriddy-d9f5.aivencloud.com:29021"
KAFKA_SSL_CAFILE=ca.pem
KAFKA_SSL_CERTFILE=service.cert
KAFKA_SSL_KEYFILE=service.key

POSTGRES_DBNAME=defaultdb
POSTGRES_HOST=pg-1-jriddy-d9f5.aivencloud.com
POSTGRES_PORT=29019
POSTGRES_USERNAME=avnadmin
POSTGRES_PASSWORD=****************
POSTGRES_SSL_CAFILE=ca.pem
```

The password has been obscured here for security reasons.  In addition, the `*FILE` env values
need to be paths to real files that contain the needed SSL key/cert configurations to
authenticate with the Aiven Kafka server.

The programs will complain if the requisite config is not set, and will specify which keys
are missing.  For info on expected types, see the [source](src/webby_check_check/config.py).

### Database setup

Once the DB creds are set up, run

```
poetry run webby-check-initdb
```

**THIS WILL ERASE ANY EXISTING DB**.  Consider using different account permissions for creation
and for regular use to avoid blowing away data.

## Usage

### Producer

To start a producer, run

```
poetry run webby-check-produce input.json
```

...where `input.json` is a list of specs for the URL poller.  An example JSON file has been
included:

```javascript
{
    "specs": [
        {"url": "https://example.com/"},
        {"url": "https://example.com/", "frequency": 300},
        {"url": "https://example.com/", "pat": "OK"}
    ]
}
```

Each _spec_ contains a required `url`, and optional `pat` and `frequency` values.  The `pat`
value is a regular expression that the poller will seek to match in the returned body of
the result.  The `frequency` parameter is the desired polling frequency in seconds.  Default
frequency is 5 minutes = 300 seconds.

### Consumer

To start a consmer, run

```
poetry run webby-check-consume
```

The consumer will run until stopped with Ctrl-C.

## Development

Development takes place on [gitlab](https://gitlab.com/jriddy/webby-check-check).

Although tests are run in CI on gitlab, they can be run locally with

```
poetry run pytest
```

Note that some tests require internet connectivity to complete.

Syntax checks and auto formatting are not currently enforced in CI, but will [become so at
a later time](https://gitlab.com/jriddy/webby-check-check/-/issues/3).  They can be run
locally with:

```
poetry run isort .
poetry run black .
poetry run flake8 .
```

### Releasing

To release, use the bumping provided in poetry, e.g.:

```
poetry version patch
git add pyproject.toml
git cm 'bump patch'
```

See `poetry help version` for more info.

### Reporting Bugs

Issues are tracked on [gitlab](https://gitlab.com/jriddy/webby-check-check/-/issues).  Please
report problems there.
